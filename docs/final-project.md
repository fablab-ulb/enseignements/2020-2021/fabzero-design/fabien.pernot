# Présentation

Mon objet se décrit simplement comme un accessoire pour pico-projecteur. En effet, cet outil pratique permettant d'amener le 7ème art dans tout les salons possède néanmoins quelques défauts qui rende parfois son utilisation complexe alors qu'il n'y a pas vraiment lieu d'être. Mon objet se propose donc de régler, ou de moins, de rectifier les défauts les plus courants des pico-projecteurs tout en offrant un objet design et épuré capable de devenir un objet à part entière dans un espace. 
De plus, cet objet est non seulement réalisable dans un fablab mais également par de nombreuses imprimantes 3D de particuliers. De ce fait, cet objet peut être obtenu par n'importe qui possédant une imprimante 3D dont les dimensions standards en permettent la fabrication. Il est donc possible pour un potentiel utilisateur de l'obtenir facilement et de le décliner comme bon lui semble en terme de coloris afin qu'il puisse s'intégrer aux goûts de tout un chacun. L'appropriation d'un tel objet est donc simple et sa fabrication de même. L'utilisateur n'a à ajouter à ces impressions qu'un peu de sable dans la balance avant de refermer cette dernière avec le bouchon et d'ajouter des aimants pour permettre le maintien des deux parties ensemble pour le transport.

![](images/présentation.png)

## Fichiers

- [balance](Fichiers/STL/balance.stl)
- [armature](Fichiers/STL/armature.stl)
- [couvercle](Fichiers/STL/couvercle_final.stl)
- [pied](Fichiers/STL/pied_final.stl)
- [socle](Fichiers/STL/socle_final.stl)
- [bouchon](Fichiers/STL/bouchon_final.stl)
- [Fichier FUSION](Fichiers/Fusion/projet_final.f3d)

# Recherches

## Objet de référence

![](images/choix_objet_projet_final.png)

**G.L. CERNUSCHI***
_Tondo Junior CT1_
Projecteur en ABS
1969

**Tondo Junior CT1** est un jouet pour enfant permettant à ces derniers de pouvoir projeter leurs propres films par l'utilisation de bobines. L'objet en lui-même se compose d'un socle plus haut que large de couleur orange permettant les branchements à l'arrière et les réglages sur les côtés, sur lequel repose une sphère blanche séparée en son centre par une bande de même couleur que le socle. La sphère supérieure est donc en deux partie : une partie contient les moteurs servant à faire tourner la bobine et certainement le circuit électrique, tandis que l'autre contient le projecteur et les roues où la bobine doit être placée. Il n'y a que cette dernière partie qui soit accessible, l'autre étant complétement bloquée. Cet objet ne laisse rien soupconné de sa fonction lorsqu'il est fermé. Il ne ressemble qu'à une sphère posée sur un socle, séparée en son centre par une ligne orange. Et là, l'objet fait preuve, pour l'époque, d'une grande sobriété et est donc déjà très contemporain avant l'heure. Mais lorsqu'on l'ouvre, on se rend compte alors, avec surprise, du réel potentiel de l'objet et de ce qu'il permet: amener une salle de cinéma dans n'importe quel foyer.

Cet objet particulier me semble tout droit sorti d'un _Star Wars_ ou du film _2001, L'Odyssée de l'Espace_ et m'a donc tout de suite plu. Hormis son air lointain de _BB-8_, j'ai pu rapidement imaginer le bruit d'une bobine de film passant dans ce mécanisme et donnant ainsi une sensation toute particulière de pouvoir se projeter dans une autre époque. Même effet qu'offre d'ailleurs la pointe en diamant d'un tourne disque sur le vinyl. Il nous donne l'impression, durant un bref instant, d' entrevoir ces époques qu'ont vécu nos parents dans leur jeunesse et nous lie en quelque sorte au passé. Bien qu'il ne s'agisse pas de notre époque, certains objets ont cet effet étrange à la manière de la _madeleine de Proust_ de nous renvoyer des images du passé ou même à des souvenirs racontés par nos parents. Quelle sensation cela devait être de mettre une bobine et de pouvoir projeter un film chez soi où qu'on le veuille. 
Son design m'a également interpellé dans la mesure où les années 60-70 ne sont pas, pour moi, mes années préférées en termes de design. En effet, malgré que ces dernières recèlent de véritables merveilles, beaucoup me repoussent par leur extravagance de forme ou de couleur et cet objet, assez épuré mais d'une grande complexité interne a immédiatement attiré mon regard. A bien y réfléchir, lorsque je regarde l'intégralité des photos prises lors de cette visite au musée, on remarque clairement que cet objet m'a tout de suite plu car c'est le seul objet que je photographie en mode portrait afin de le séparer des autres, de son contexte temporelle pour ne garder que lui et le mettre en avant. Sans doute déjà, une décision inconsciente de ma part.


### Modélisation 2D et 3D 

La modélisation a déjà été un bon point de départ de l'analyse car dans la mesure où cet objet est assez petit, on est forcé, pour réaliser une modélisation conforme, de faire attention et de passer devant des détails de construction tels que les mécanismes d'ouverture, les emboîtements ou encore les différents boutons de réglages. Cela a notamment été permis grâce à la multitude de photos existantes des différents modèles qui sont d'ailleurs actuellement toujours prisés par les collectionneurs.
On peut ainsi, en voyant dans le détail de la reproduction, voir comment a fait le designer afin de conserver l'esthétisme souhaité tout en permettant la technicité.

![Modélisation sur **Fusion 360**](images/montage_2D_et_3D.png)

Modélisation sur **Fusion 360**

![Impressions 3D sur **Prusa**](images/impression_3D.png)

Impressions 3D sur **Prusa**

### Contexte et Design

#### Social

Pour comprendre un objet, il faut se demander pourquoi le designer a créé cet objet, pourquoi a-t-il cherché a modifier l'espace de vie de sa société en offrant un objet plutôt qu'un autre. Cela permet de comprendre pourquoi cet objet était bien pensé, comment le designer en a eu l'idée et pourquoi il a choisi ce design plutôt qu'un autre. 
Pour le **Tondo junior CT1** nous sommes alors en 1969. La télévision est encore un luxe et n'offre pas beaucoup de posibilités dans les foyers puisque les choix des programmes sont très restreints. En effet, en Italie, lieu de création de l'objet, seules deux chaînes existent à savoir la _Rai 1_ et la _Rai 2_. Il faut comprendre également qu'en Europe, la télévision en plus d'être un luxe est aussi bien moins utilisée à l'époque que maintenant. Cette dernière est allumée uniquement pour les infos et parfois le soir pour un film mais ce n'est pas dans l'état d'esprit des gens de se divertir de la sorte. En plus, ces programmes, le plus souvent limités, offrent des divertissements pour enfants en noir et blanc mais pas de vrais films en couleur comme il est facile d'en trouver maintenant. La seule manière de voir des films et de se rendre au cinéma, lieu réservé et exclusif pour la projection des grandes oeuvres cinématographiques. La plupart de nos parents n'ont d'ailleurs vu leur premier _Walt Disney_ qu'à l'âge de 8 ou 9 ans ce qui nous paraît impensable à présent. Peut être **Cernuschi** voulait-il rendre possible la visualisation de grands films à la jeunesse pour qui les programmes de l'époque étaient restreints. Mais alors pourquoi la lecture en 8 mm et en super 8 ? Les super 8 sont utilisés à cette époque dans le domaine des courses automobiles afin de filmer et voir les temps des participants ce qui a dût marquer et influencer G.L Cernuschi. En 69, Le 8mm est quant à lui le système de cinématographie le plus courant de l'époque et est présent dans les caméras déjà lors des années 50, caméras qui ne sont d'ailleurs pas démocratisées et donc accessibles à tous. Mais sachant que le super 8 ne date que de 1965, la question ici est de savoir comment ce designer a pu prévoir l'essor du super 8 et le fait que ce dernier s'installe ensuite dans les foyers moyens comme preuve de leurs réunions festives lors de sa démocratisation dans les années 70-80. De plus, que ce soit le 8 mm ou le Super 8, ces bobines sont réservées à l'époque à des élites alors pourquoi en faire un jouet pour enfant ? 

Un autre contexte social est à prendre en compte dans la réalisation de cet objet qui est la révolte étudiante présente à travers toute l'Europe où cette jeunesse cherche à se défaire des carcans de la génération précédente pour se réapproprier l'espace. Les designers vont alors suivre cette dynamique en proposant des objets facilement transportables ou mieux, en transformant les objets dit "figés", symboliques de cette génération précédente ancrée dans ses traditions, en objet manipulables. C'est à cette période que les objets peuvent bouger de place facilement et que l'on fait bouger l'objet lui-même par des systèmes d'ouvertures et de fermetures, des parties qui se déplient,... On assiste alors à la création de l'objet "mouvant" offrant une grande ingéniosité. On est dans le mouvement en se tournant vers des objets plus légers dont le poids est assuré techniquement grâce à l'utilisation du plastique. Cela peut être a-t-il poussé Cernuschi à réaliser un objet pour les plus jeunes afin que ces derniers se réapproprient les espaces.


#### Historique

Il ne faut pas non plus négliger la part importante du contexte historique dans le design même de l'objet. En effet, cet objet date de 1969, année où l'Homme marchera pour la première fois sur la Lune et s'inscrit alors dans une lignée d'objet s'inspirant de cette engouement pour la conquête spatiale avec des formes plus rondes et organiques mais qui peuvent parfois se voir affinées pour devenir aérodynamique. Mais cette frénésie pour les formes "spatiales" date en réalité, à ses débuts, des années 50 où de nombreuses publicités américaines prônaient les bienfaits de la vie dans le futur grâce à l'utilisation de l'énérgie atomique et de la colonisation spatiale. Ces formes se retrouvent déjà d'ailleurs dans une autre création de **G.Lurani Cernuschi** qui est la voiture "_Nibbio 2_" qu'il réalise en 1956. Et on remarque très bien ici une volonté de réaliser un design spatiale par une forme simple et épurée, en rondeur, avec des couleurs plutôt sobres pour une époque où le **Pop Art** connaît portant ses heures de gloire. En effet, seuls le socle et une bande centrale, tous deux oranges, viennent casser le blanc de la forme.
et c'est la tout le paradoxe de cette époque qui joue entre les formes et couleurs très fortes et saturées du **Pop Art** et le design épuré, sobre et blanc de l'influence spatiale. Cet oeuvre semble avoir fait le choix de se ranger du côté du design spatial et pourrait être une référence au film _2001 L'Odyssée de l'Espace_ de **Stanley Kubrik** sorti en 1968 ce qui serait d'autant plus impressionnant étant donné la fonction de l'objet.

![](images/tondo_2001_l_odyssée_de_l_espace.png)

#### Culturel

L'objet étant réalisé par un designer italien, les réponses à plusieurs des questions que l'on se pose peuvent se trouver dans l'étude de la culture italienne et notamment dans l'importance de la relation qu'entretient l'Italie avec le cinéma. En effet, les années 65 à 70 sont réputées pour voir émerger de nombreux réalisateurs italiens talentueux. Mais alors d'où vient cet engouement de l'Italie pour le cinéma ?
L'engouement pour le cinéma fut immédiat en Italie avec déjà des premières productions cinématographiques dès le début des années 1900 avec en 1910 l'avénement des "**divas**". Ces productions vont ensuite se poursuivre tant bien que mal jusqu'à la montée du fascisme qui va alors permettre un développement du cinéma aussi bien dans le domaine populaire que dans le domaine de l'éducation dans un but de propagande.
C'est en effet au début du XX ème siècle que le cinéma apparaît comme un moyen d'apprentissage performant et ludique dans les écoles italiennes, de manière modeste d'abord, dans le but d'éduquer la jeunesse populaire. Le tout avec l'accord des partis socialistes de l'époque et même un recours financier du Gouvernement italien pour développer les différentes écoles souhaitant participer à l'entreprise. Ce dernier n'est en réalité que la suite logique du mouvement éducationnel de l'époque qui cherche alors à renouveller son système d'apprentissage par des objets plus visuels qui deviennent alors des supports plus ludiques et parlants pour les élèves. C'est d'ailleurs lors de ce dit mouvement qu'apparaissent les cartes et les photographies dans les salles de classes italiennes et effectivement, par extension, les projections cinématographiques. 
Mais bien que l'enthousiasme pour les projections cinématographiques dans le domaine éducatif soit présent et bien partagé, il ne se ressent pourtant pas dans les faits puisque les écoles restent mal équipées voire non équipées et ce jusqu'en 1922 avec l'arrivée au pouvoir du fascisme avec Mussolini et surtout par la nomination au poste de Ministre de l'Instruction Publique de Giovanni Gentile qui va être le premier à mettre sur pied un projet de grande envergure afin d'équiper les écoles d'Italie à une éducation par le cinéma. Bien entendu, avec la montée de Mussolini se pose alors la question grandissante de la propagande dont l'éducation par le cinéma devient alors un outil de choix pour former les futurs esprits dès le plus jeune âge. Cet état de pensée va donner naissance au milieu des années 20 au fameux studio **LUCE** chargé de réaliser les différents films de propagande et qui deviendra d'ailleurs, au fur et à mesure de la montée en puissance du régime fasciste, le seul producteur de films. La **LUCE** est alors chargé de produire des films de propagande aussi bien dans les salles de cinéma classiques que dans les salles de classes italiennes. Le cinéma dans l'éducation se développe donc largement, fort de ces moyens, à travers toute l'Italie mais reste tout de même assez compliqué étant donné le coût de telles mis en place et la théorie reste assez éloignée de la pratique tandis que les années 30 marquent un développement conséquent dans le cinéma classique toujours grâce au fascisme.
Il y a donc toujours eu un lien très fort entre Italie et cinéma. Lien que l'utilisation du cinéma dans l'éducation a accentué notamment dans un but de propagande, certes, mais qui a continué d'entretenir un engouement pour le cinéma et cela même après la chute de Mussolini. En effet, le cinéma devient même, dans les années 50-60, une part essentielle du quotidien des italiens où ce dernier est omniprésent par une production colossale d'oeuvres mais également d'affiches comme en témoignent les oeuvres de l'artiste **Mimmo Rotella** qui s'approprie alors ces éléments de la vie quotidienne des Italiens que sont les affiches pour ensuite les transformer et les retravailler mais par un jeu de _décollage_ cette fois-ci.

![](images/mimmo_rotella.png)

C'est essentiellement dans les années 60 que les registres cinématographiques se multiplient notamment grâce à la fin du fascisme et à la "cicatrisation" du pays. Cela signifiant une libération du cinéma et des thèmes abordés, et l'appararition en particulier du cinéma d'auteur qui fera la renommée internationale du cinéma italien. Durant cette même période, subsiste en parallèle du cinéma d'auteur reconnu mondialement, un cinéma populaire s'étendant à divers registres et qui connaît un franc succès. Aussi appelé "_cinéma de quartiers_" ou encore "_cinéma Bis_", ces films sont bien plus nombreux que les films d'auteur dans le sens où ils n'ont pas la même renommée ni les mêmes prétentions et donc coûtent bien moins chers avec des scénarios assez simples et des durées de tournages très courtes. De cette façon, ces derniers fleurissent des années 60 aux années 80 et deviennent tout de même cultes au fil des années en offrant tout les critères du pop art américain par les jeux de couleurs et une libération des moeurs. 

![](images/ciné_populaire.png)

Ce cinéma fait même l'objet à présent de différents hommages à travers l'émission **Stracult** animé par Marco Giusti ou encore la BD **Midi-Minuit** de Doug Headline et Massimo Semerano.

![](images/hommages.png)

En Italie, les programmes pour enfants sont très limités et se résument en réalité pour ce qui est de la télévision essentiellement à l'émission **Carosello** diffusée des années 50 aux années 80 sur la chaîne **Rai 1** pendant 10 minutes le soir et se servant surtout de personnages d'animation dans le but de vendre des produits. On assiste alors aux débuts des publicités avec des personnages enfantins qui deviendront par la suite célèbres, pour certains, comme ce fut le cas par exemple pour **Calimero** qui, au départ, vantait les mérites d'une marque de savon. Mais comme il a été déclaré précédemment lors du parallèle entre le **Tondo CT1** et la télévision, ces programmes télévisuelles pour enfants sont assez limités. Il faut alors se tourner vers des courts-métrages d'animation qui eux, forcément moins coûteux que les long-métrages, se développent durant les années 60 en Italie et permettent également aux jeunes générations d'apprécier les joies de la projection cinématographique. On peut citer comme exemple typique d'animation de ces années, la création de Bruno Bozzetto, **Signore Rossi**, qui se retrouvera à travers différents court-métrages dans les années 60 et fera même l'objet dans les années 70 de long-métrages. 

![](images/rossi.png)

Bruno Bozetto, bien qu'il ne soit pas le seul à se lancer dans cette entreprise, représente véritablement la figure importante du cinéma d'animation de ces années en Italie et permis alors, par son travail, d'inspirer d'autres artistes qui se lanceront par la suite dans ce domaine cinématographique. De plus, on peut émettre l'hypothèse que de nombreux autres courts-métrages animés soient parvenus en Italie grâce aux commerces de film 8mm ou **Super 8** venant d'autres pays d'Europe ou même des Etats-Unis.

### Créateur

**Giovanni Lurani Cernuschi** est le designer italien responsable du **Tondo CT1**. Ce dernier n'est pourtant pas connu spécialement pour ces objets de design car en effet il a également designer 2 voitures ainsi que leur moteurs (_"Nibbio"_ et _"Nibbio 2"_) mais plus pour ses qualités de pilote et d'ingénieur. En effet, ce dernier était un véritable fan d'automobile et un coureur émérite ce qui lui a valu de participer à plusieurs courses prestigieuses au nom de grandes enseignes automobiles et d'écrire différents ouvrages au cours de sa vie sur le monde automobile de l'époque. Né en 1905 il a donc 64 ans à l'époque où il crée le **Tondo CT1**. 

### Editeur

**Polistil** ou **Polistoys** est une agence italienne spécialisée dans le modélisme automobile. Fondée en Italie à la fin des années 50, elle se spécialise dans les reproductions les plus fidèles possibles de voitures avant de se diversifier par la suite dans la production de modèles de motos et de camions. Elle connaît une grande notoriété grâce aux différents mécanismes qu'elle rend possible dans ses reproductions (sièges basculants, portières ouvrables,...) et qui sont d'une extrême finesse et réclame des savoirs-faire minutieux. Cependant, cette société disparait en 1993 pour ensuite refaire son apparition en 2005 comme appartenant à un groupe chinois nommé May Cheong Group. 

A cette époque, et comme dit précédemment, la télévision n'était pas très répandue et la publicité fonctionnait alors différemment. En effet, ce n'est pas par le biais de spots publicitaires que les enfants pouvaient voir les jouets qu'ils désiraient mais par le biais de catalogues de vente qui montrent deux faits importants (en tout cas pour ceux de **Polistil**). Le premier est que bien que le plastique ne soit apparu dans les foyers moyens grâce au design que dans les années 70, ce dernier occupait déjà une part très importante dans la production de jouets puisqu'en voyant les différents catalogues de différentes marques de l'époque, on se rend bien compte que les moulages et techniques pour faconner le plastique étaient déjà maîtisés par les constructeurs de jouets. D'ailleurs, en considérant cela, il n'est pas étonnant que le **Tondo CT1** soit si bien maîtrisé et pure puisque le designer a alors collaborer avec un éditeur pouvant parfaitement répondre à des attentes précises et minitieuses dans le travail du métal et du plastique comme en témoignent les catalogues de l'époque très imprégnés par la culture pop art.

![](images/fascicule1.png)

De plus, à cette époque, la marque **LEGO** produit déjà des modèles en plastique en grande quantité grâce à des moules. Cela montre que les fabricants de jouets possèdent les savoirs-faire nécessaires à la production de plastique en masse, ce que les designer ne savent pas encore faire. Pas étonnant que Cernuschi ait pu réaliser à grande échelle un objet design en collaborant avec **Polistil**.

Mais la question est alors de comprendre comment cette collaboration a pu voir le jour ? 

Le second fait marquant que l'on remarque en parcourant ces magazines et même ceux des années suivantes et la singularité du **Tondo CT1** par rapport au reste du catalogue. En effet, **Polistil** s'étant spécialisé dans la fabrication de modèles réduits automobiles de grande qualité, il est plutôt surprenant d'observer en pleine page un projecteur. De plus, le **Tondo CT1** n'est que le premier d'une longue série de **Tondo CT** qui offriront au fur et à mesure de plus en plus de films et d'accessoires tout en conservant le même modèle esthétique de base. Ils évolueront cependant pour offrir de nouvelles fonctionnalités.

Alors pourquoi **Polistil** a-t-il produit un projecteur là où la fabrication de voitures était déjà maîtrisée et où le designer était un passionné d'automobile ? D'où vient cette idée directrice du projecteur ? D'ailleurs, cette différence se retouve également au niveau du packaging. En effet, les publicités de **Polistil** de cette époque suivant le modèle du **Pop Art** pour les reproductions automobiles dénotent avec les publicités mises en place pour le **Tondo CT1** ce qui laisse à penser que ce packaging a été pensé par le designer dans le même style spatial que l'objet.

![](images/affiche.png)


### Innovation

Le projecteur **Tondo CT1** est le premier d'une lignée qui va se retrouver jusqu'à la fin des années 70. En effet, il sera ensuite suivie par d'autres modèles possédant plus d'accessoires et de films (jusqu'au Walt Disney) conservant tout de même le design original pensé par **Cernuschi** malgré quelques ajustements dans la configuration du projecteur et les différents coloris disponibles. Mais il est important de remettre cet objet dans son contexte afin de comprendre pourquoi cet objet est si particulier. On se doit alors de voir les différents projecteurs existants avant et après afin de se faire une idée de l'intérêt qu'à susciter le **Tondo CT1**.
Dans les années précédants cet objet, on remarque en Europe et particulièrement en Italie, une fabrication plus importante du nombre de projecteurs, témoignage que les bobines de films de 8mm commençait peu à peu à se démocratiser tout comme l'utilisation des caméras. Malgré tout, bien que ces outils ne soient plus réservés à une élite, il sont tout de même dans un "début" de démocratisation, c'est à dire qu'ils sont quand même réservés à une classe sociale aisée. Si l'on part par exemple du début des années 60, on peut trouver alors en 1961 le **Projecteur de film 18 5** de Bolex Paillard qui malgré son aspect solide et lourd est tout de même transportable. Il est entièrement fait de métal et semble être la première véritable forme de projecteur qu'il soit "pratique" de déplacer. Sa ressemblance non dissimulée à une valise accentuent ce caractère et on voit déjà là une volonté certaine, grâce aux objets qui peuvent se plier et se refermer, de réduire voire même de miniaturiser les projecteurs autrefois si imposants. Vient ensuite en 1965 le **SUPER 8T** de Noris qui cherche lui aussi à rendre facilement transportable un projecteur entièrement réalisé en métal et donc assez robuste et lourd avec un design plus carré et stricte que le **Projecteur 18 5** qui semblait pourtant déjà plus petit que ce modèle. Le **MIRAGE C-200 SP** de Chinon mis en vente l'année suivante semble lui plus pratique avec sa forme allongée. Cependant, l'objet et toujours entièrement en métal et les pièces de machinerie internes restent volumineuses et lourdes. On cherche à miniaturiser et transporter mais le métal et la mécanique même du projecteur qui n'est pas encore réduite ni traitée avec finesse rendent complexe cette tâche. On voit également apparaître les visionneuses de films qui permettent en réalité de lire des films en les regardant à travers un écran ou la source lumineuse se trouve devant nous là où les projecteurs permettent d'avoir la source lumineuse plutôt derrière. C'est par exemple le cas de la **Visionneuse SB1** commercialisée par Braun en 1968 qui, en réalité, ressemble à une télé en petit format. Son enveloppe est là encore entièrement métallique et semble encore assez lourde même si on remarque quand même une diminution de la taille. Ce n'est qu'en 70 qu'on remarque alors la **Visionneuse F1** de Bauer avec une coque en plastique. Malgré cela, ce modèle ressemble beaucoup à la **Visionneuse SB1** dans sa forme très carré avec un socle imposant par rapport au reste de l'objet mais on est bien là dans un modèle réduit facilement manipulable. Et pour cause, ce modèle est très réduit par rapport à ces modèles de 68 et facilement transportable et manipulable parcequ'il n'est pas motorisé. Il est donc manuel ce qui permet un gain d'espace au niveau de la machinerie interne mais prend de la place une fois ouvert là où le **Tondo CT1** se cantonne à un espace très restreint. Les années suivantes, les premiers projecteurs sonores font donc leur apparition et avec eux un retour au modèles transportables en métal et avec des dimensions assez contraignantes malgré la possibilité de pouvoir les déplacer comme par exemple avec le modèle **P6-24 B sonore** de Heurtier datant de 1972.

Cependant, tous ces objets de différents pays ne font pas partie du paysage italien, pays de fabrication du **Tondo CT1** et on observe déjà de grandes différences selon les pays puisque, là où dans le reste de l'Europe les vidéoprojecteurs sont encore massifs et en métal, on peut trouver des vidéoprojecteurs en plastique dès 1966 en Italie et qui plus est motorisés là où les autres projecteurs ne le sont pas forcément même plus tard. Pour cette étude, j'ai préféré étudier la marque **Silma** qui est une marque italienne reconnue déjà à cette époque. Le dernier modèle en métal semble dater de 1965 pour cette marque. On peut citer comme exemple le **120 Super Zoom**  de **Silma** qui va multiplier les créations de projecteurs durant les années 60-70. Ce modèle par exemple est fait de plastique et possède déjà des dimensions assez restreintes dans l'objet et dans la mécanique qui la compose. On remarque également que la marque **Silma** va, dès 1967 produire 3 modèles différents offrant tous la possibilité d'être transporté mais surtout de lire le **Super 8** qui pourtant n'est créé qu'en 1965. Le premier modèle lisant le **Super 8** est d'ailleurs le **120 M** de 1966. On est alors témoin de l'importance qu'a le cinéma pour les italiens à cette époque et cette volonté d'être toujours plus performant dans ce domaine avec des projecteurs plus petits, plus rapides et plus techniques. De plus, là où les autres pays obtiennent des projecteurs avec son en 1972 très imposants, on peut retrouver un projecteur de Silma de 1970 équipé de son nommé le **Bivox D LUX** ou même en 1968 avec le **S250** entièrement métallique cette fois, ou encore en 1963 avec le **Sonik 8** qui ,lui par contre, est plus compliqué à déplacer. Le **Bivox D LUX**, malgré le son, reste tout de même très compact ce qui encore une fois prouve le savoir-faire des italiens en matière de création de petites pièces et de précision. Mais le son est encore peu développé à l'époque, pour preuve les autres modèles suivants qui sont pour la grand majorité sans son. On remarque d'ailleurs avec le son un retour au métal, bien que partiel, cherchant peut être à protéger les hauts parleurs et le système sonore. Cependant, en 1971, la marque Silma, avec le modèle **S222 Sound** lance un projecteur munie de son et avec une enveloppe entièrement plastique. 

![](images/frise_chronologique.png)

On observe alors au travers de ces différents modèles qu'en Italie le cinéma et la vidéo ont, durant ces années, une place plus importante que dans le reste de l'Europe. On remarque également que le son est une préoccupation et cela dès 1963 pour les producteurs de projecteurs. Mais cet objectif sera remplacé ensuite par un soucis de miniaturisation pour améliorer le transport des projecteurs. On remarque également un intérêt immédiat pour le **Super 8** qui se développe alors très rapidment dans ce pays et une utilisation assez précoce du plastique dans les objets. Le son qui est un peu délaissé après 63 refait son apparition dans différents modèles à partir de 68 avec des parties métalliques protégeant les installations sonores. Mais dans les années 60, on réalise clairement que le but principal et de rendre les projecteurs transportables et de tailles les plus réduites possibles. Cela explique pourquoi **Cernischi** utilise aussi facilement le plastique et met en place une lecture **Super 8** dès 1969. En comparaison de ces différents modèles, le **Tondo CT1** est un objet qui ne dévoile pas sa fonction là où les autres projecteurs de l'époque signale clairement leur statut. Il est, de plus, de taille plus réduite que les autres projecteurs de l'époque et offre donc un objet à part entière à l'esthétisme pure contrairement aux projecteurs sur le marché de l'époque.

### Détails de la structure

#### Matières

![](images/dessin.png)

##### ABS

L'**ABS** est en grand pas dans le design dans la mesure où c'est un matériau qui peut être facilement moulé dans le but de créer des formes toutes en rondeur ou des coques ce qui effectivement se prête bien à cet objet. Les coques sont utilisées des deux côtés et sont probablement moulées toutes les deux. De plus, l'**ABS** offre un côté lisse et brillant à l'ensemble de l'objet. Ainsi, on a une impression d'intégralité de l'objet ou du moins de la sphère car cette derniere semble être constituée d'un seul bloc et ne pas pouvoir s'ouvrir. C'est une entité pleine, une sphère complète posée sur un socle. Il semble alors que l'on puisse désolidariser la shère de ce socle et chercher à comprendre quel est cet objet.

##### Métal

Le **Métal** est ici utilisé bien évidemment pour le moteur mais également pour le socle qui permet ainsi à ce dernier de tenir.  En effet, les différents tests réalisés en impression 3D montrent bien que l'objet ne peut pas tenir à moins d'équilibrer la charge des deux côtés ce qui rendrait alors probablement l'ouverture et l'accès au projecteur plus complexe, ou alors de ne mettre du poids que dans la partie inférieure, autrement dit le socle, pour maintenir l'objet stable où qu'il soit. Le métal est également utilisé pour la poignée de transport sur la partie haute de la sphère qui se rétracte ensuite dans la partie centrale orange. En métal simplement pour une question de solidité évidente. 

#### Système d'ouverture et de rotation

Cet objet, bien qu'il soit un jouet pour enfant possède tout de même une grande complexité. En effet, différents réglages sont présents sur l'objet afin d'offrir une qualité de visionnage. Les enfants de l'époque devaient alors comprendre comment ouvrir, régler l'image et mettre en place les films dans l'objet. Mais malgré l'ingéniosité et la complexité de l'objet, l'utilisation reste assez simple permettant alors à n'importe qui de pouvoir se servir d'un objet de grande précision. Comme dit précédemment, dans cette époque les objets fixes entrent en mouvement par leur poids qui diminue grâce à l'utilisation du plastique mais également grâce aux systèmes d'ouvertures et de fermeture qui permet aux designers de jouer avec les objets en cachant et révélant les fonctions. Cet objet ne déroge pas à la règle et se compose alors de deux coques en **ABS** qui ne s'ouvrent pas mais peuvent se retirer complétement. En effet, là ou les objets de l'époque peuvent s'ouvrir mais restent fixes à leurs jonctions, ce dernier possèdent des parties que l'on peut totalement enlever afin de laisser l'objet dans sa forme la plus simple à savoir la partie orange soutenant le projecteur et le moteur. Il y a donc une possibilité de séparer les coques de l'ensemble là où les autres objets des années 70 ne le permettent pas forcément.

#### Mécanisme du vidéo-projecteur

La complexité et la miniaturisation du système mécanique du vidéoprojecteur peuvent s'expliquer soit par les connaissances d'ingénieur du designer qui peuvent nous laisser penser que ce dernier s'est également occupé de la partie technique de son objet, soit par l'éditeur lui-même qui possédait les capacités de produire ce mécanisme par le biais d'ingénieurs et de techniciens travaillant sur des systèmes de grande précision dans les reproductions automobiles de la marque **Polistil**. En effet, cette marque s'étant démarquée de ses concurrents en offrant à sa clientèle des reproductions d'une grande qualité, certes, mais également des systèmes mécaniques subtils tels que l'ouverture des portes du véhicule, les sièges battants, la rotation du volant entraînant le mouvement des roues,... On peut alors supposer que les employés même de l'entreprise possédaient les connaissances suffisantes à la réalisation d'une telle machinerie. Tout comme **Polistil**, maitrisant déjà les moules d'ABS, a pu produire ce projecteur à grande échelle en offrant au créateur des outils que le monde du design ne connaissait pas encore.

## Réinterprétation

Après un questionnement, l'intérêt était de savoir quel objet produire. En effet, le virtuel semble être l'innovation de notre époque et il s'agissait alors de choisir un objet en rapport avec cette thématique. Mais après des recherches assez large sur ce sujet, j'ai finalement opté pour conserver l'idée du rétroprojecteur qui est toujours d'actualité étant donné que le cinéma n'a jamais été autant accessible depuis l'ère du numérique et des différentes plateformes vidéos. On remarque d'ailleurs de plus en plus, un retour au rétroprojecteur au détriment des télévisions qui n'offrent plus réellement de programmes adaptés. En effet, les consommateurs d'aujourd'hui regardent de moins en moins les programmes télévisuels au profit des plateformes vidéos puisque ces derniers permettent à n'importe quel moment d'avoir un programme choisi par le consommateur. Le tout était alors de savoir comment transformer notre objet de référence pour le ramener à notre époque contemporaine.

### Premières esquisses

![](images/DESSINS.png)

Les difficultés d'un tel travail sont en réalité de retranscrire un objet et ceux avec les même codes que le designer initial. Ici, à une époque où les rétroprojecteur sont de formes assez basiques et carrée, le designer a justement pris le contrepied en offrant un objet sphérique. On peut alors jouer sur cette partition qu'il réalise de la sphère pour y insérer son orange métallique et partir de la même base afin d'offrir une réponse. On amène alors un objet différents en affinant le cercle et en ne conservant qu'une partie que l'on vient cette fois rendre en orange. Dans le même procédé que pour le **Tondo CT1**, on assiste là aussi à un problème de poids qui est réglé par le poids amené par le socle de couleur blanche à présent et qui ne se divise plus en deux aspects différents (un bloc bas et une bande centrale) mais bel et bien en une seule et même forme qui intègre d'ailleurs le système sonore du modèle. L'innovation qui m'a le plus marqué dans l'objet de référence est la capacité du projecteur à pouvoir effectuer une rotation sur l'axe Z. Cette pratique me semblait efficace et utile dans une époque qui est la notre où régler un projecteur sur la hauteur et toujours compliqué. Peut-être alors que ce système peut être conserver afin de facilité la vie de l'utilisateur. Mais pour cela, le cercle affiné créé devrait être placé en hauteur afin de pouvoir effectuer sa rotation sans encombres. Ainsi, le socle devrait pouvoir permettre de soulever cette partie de sphère en permettant le maintien de l'objet. Les premiers essais répondaient à cette attente mais ne créés pas d'unité dans l'objet final. En effet, il s'agissait de deux formes attachées mais non imbriquées l'une dans l'autre ce qui a donné naissance à la seconde idée de socle possible. Ce dernier permet également non seulement d'incoprporer le système audio mais également de dissimuler simplement les ports électroniques mais aussi d'offrir des aérations adéquates sur l'arrière de l'apparail. En effet, après une analyse d'autres vidéoprojecteurs, il se trouve que des ouvertures pour les systèmes audio sont nécessaires mais également des systèmes de ventilation. Les systèmes de ventilation sont en réalité placés au niveau du projecteur qui chauffe inévitablement par la lumière mais également au niveau du circuit imprimé. Il a fallu, une fois le problème des ventilations du circuit imprimé réglé par le nouveau socle, penser à un moyen d'aérer le projecteur lui-même. Pour ce faire un système de déploiement est possible sur la partie contenant le projecteur afin d'en permettre le refroidissement. Tout cela bien sûr ne sont que des hypothèses qui, si elles sont validées, seront ensuite modélisées via **Fusion 360**.

### Qu'est ce que le cinéma aujourd'hui ?

Après une reflexion, il s'est avéré que modifier mon objet n'était pas la bonne solution dans le cadre où cela revenait finalement à un travail classique de **Design Produit** et que je ne possédais pas les connaissances nécessaires afin de réinterpréter l'objet à proprement parler avec tout l'électronique et les différentes contraintes techniques qu'il soulevait. Il a fallu alors se demander qu'est ce qu'est le cinéma au jour d'aujourd'hui dans notre société contemporaine?
Avec le développement et le succès incroyable des nouvelles plateformes vidéos tels que **Netflix**, **Disney +** ou encore **Amazon**, les films récents sont très rapidement disponibles en visionnage et les anciens sont retrouvés pour être mis en ligne sur ces plateformes vidéos permettant de visionner un film en excellente qualité n'importe où et n'importe quand mais surtout selon le choix du client. Ainsi, par le virtuel, deux branches se développent particulièrement dans le domaine du visionnage vidéo: les vidéoprojecteurs et les matériels VR permettant de voir des films dans une ambiance particulière. Il est vrai que l'expérience du cinéma est quelque chose que l'on ne peut encore qu'approcher mais il n'en demeure pas moins que ces pratiques et leurs perfectionnements pourrait changer le cinéma. Chacun de ces deux modes possèdent des atouts et des contraintes bien particuliers.
En effet, les lunettes et casques VR permettent effectivement une immersion plus profonde dans l'ambiance d'un cinéma à proprement parler puisqu'ils nous coupent de notre environnement extérieur mais la lumière aussi forte et proche des yeux et une sensation qui devient désagréable assez rapidement. La sensibilité du cinéma se retrouve alors mais l'inconvénient est alors que pour se couper de l'environnement extérieur, il faut alors accepter de regarder ses films seul. Malgré cela, les ventes de ces outils numériques ont subi un véritable bond notamment grâce au domaine du gaming qui est leur principal domaine d'utilisation.

![](images/global-vr-headset-market_2.png)

Les vidéoprojecteurs quant à eux ne rendent pas cette ambiance aussi fortement mais permettent de partager l'expérience avec plusieurs personnes (amis, famille,...) et l'éclairage est moins aggressif tout en permettant de nos jours des qualités de rendus en HDR. Cependant, ces derniers ont également des défauts dans le réglage de la hauteur par exemple mais aussi dans les différents branchements qu'ils obligent et la mise en place qu'ils demandent. On remarque de plus en plus un engouement pour ce type d'articles qui peuvent à présent se placer près du mur sur lequel ils projettent et offrent des tailles d'écran qu'une télévision ne peut pas offrir. Des améliorations ont été apporté au fil des années afin d'éviter les différents reflets et une amélioration des couleurs pour une plus grande qualité d'image "non numérisée". 
On remarque également une branche des vidéos projecteurs en pleine expansion qui est celle des **pico-projecteurs** qui, durant ces dernières années, a su se développer dans le sens où elle permet une miniaturisation des projeteurs qui possèdent alors une autonomie par l'intermédiaire de batteries, des stockages et des haut-parleurs intégrés qui malheureusement, ne possèdent pas une qualité de sons encore assez bonne mais peuvent être reliés à des enceinte via le Wi-Fi.
Ces deux outils offrent des niches économiques importantes et sont promis à une expansion mondiale.

![](images/Global-Advanced-Cinema-Projector-Market2.png)

Il est donc important de rester sur le vidéoprojecteur dans cette réinterprétation de l'objet. D'autant plus que cet objet de référence aurait finalement tout à fait sa place dans notre société contemporaine étant donné que l'objet en lui-même, à savoir le vidéoprojecteur subi encore une expansion de nos jours et fait de plus en plus partie des foyers populaires. Mais ces projecteurs actuels, qui deviennent d'ailleurs des pico-projecteurs, ont tout de même quelques problèmes et,comme dit précedemment, je n'ai pas les compétences nécessaires afin de créer un nouveau projecteur mais peut être est-il possible de créer un accessoire permettant de pallier aux problèmes de ces pico-projecteurs. Ces derniers sont en réalité la mauvaise qualité sonore qui peut être facilement réglée par l'utilisation d'une enceinte mais alors peut être créer un support pour cette dernière ? De plus les fils sont toujours un problème avec les projecteurs qu'ils soient miniaturisés ou non mais également la ventilation qui doit être conservée et préservée surtout. L'angle du projecteur est aussi assez complexe à régler et c'est certainement le plus gros défaut qui demande le plus souvent de se procurer un trépied réglable. Mais les pico projecteurs permettent des retranscription en HD et offrent alors des distances raisonnables de projection avec une qualité d'image certaine et font alors l'objet de ma réinterprétation. De plus, ces derniers sont le futurs des vidéos projecteurs qui se voudront toujours plus petits et performants.

### Esquisses de recherches

L'intérêt de ces pico-projecteurs, hormis leur taille réduite et leur prix, est leur qualité esthétique dans la mesure où, contraiement aux autre projecteurs, il n'est pas nécessaire  de les cacher et certains mérites même d'être mis en valeur. A voir...
Cependant il n'en reste pas moins que si certains sont agréables à regarder, après une étude des différents modèles, certains ressemblent beaucoup à des modèles réduits de vidéoprojecteurs normaux et sont donc peu esthétiques. Ainsi, le mieux est de partir du postulat de les dissimuler. 

![](images/forme_1.png)

Ainsi par ce procédé, les rotations peuvent être assuré selon 2 axes de rotation pour permettre un ajustement plus simple du vidéoprojecteur et les problèmes de fil "laissés libres" disparaît d'autant plus que seul deux fils sont généralement nécessaire (1 pour l'alimentation même si l'appareil possède une batterie et 1 pour le fil reliant l'ordinateur). Cependant un autre moyen est peut être possible afin d'améliorer cela... Il faut ensuite penser au transport de l'objet et également comment "bloquer" le vidéoprojecteur à l'intérieur de cet accessoire. 
il faut également penser à l'aération et au transport de l'objet avec et sans le vidéoprojecteur, ainsi qu'au système de rotation et à comment bloquer la partie blanche. Un système basé sur la rotation ? ou peut être séparer la partie blanche en deux et relier les deux parties à une autre par le moyens de clips comme on peut voir sur les télécommandes ?

![](images/essai_2.png)

![](images/essai_3.png)

### Une mise à distance avec l'objet de référence

Il semble important, après ces échecs, de reprendre le problème à sa base. En effet, une trop grande connaissance de l'objet de référence empêche parfois de prendre le recul nécessaire à la création d'un nouvel objet amenant une autre fonction. De plus, l'objet de base possédant une multitude de complexité aussi bien dans son mécanisme que dans ses systèmes d'ouvertures et de fermetures m'empêche d'agir avec simplicité. Il va alors s'agir tout d'abord de redéfinir les attentes de ma réinterprétation avant de me lancer dans un objet offrant le plus de simplicité possible. 

L'objet en lui-même doit être réalisable facilement et simplement par les outils de Fablab à disposition. Il doit servir à améliorer l'utilisation des **pico-projecteurs** qui sont dorénavant une niche économique en plein essor et qui sont donc de plus en plus démocratisés et utilisés. Les **pico projecteurs** se définissent par leur petite taille et après une recherche et une comparaison de différents modèles, on peut estimer leur taille entre **8x8x3** et **16x10x5** pour un poids compris entre **300g et 1kg** maximum. Au-delà de ces dimensions, on entre alors dans les **mini-projecteurs** puis dans les **modèles standards**.

- 15x8x2 230g
- 11x9,8x3,8 350g
- 9,8x9,8x2,2 213g
- 11x10,4x2,7 300g
- 6,2x5,2x4,8 136g
- 12,4x8,3x4,6 
- 9,2x8,1x5,5 
- 7,6x7,1x3,4 215g
- 14,5x8x1,8 286g

Les dimensions sont donc tellement variables qu'il vaut mieux prendre les extrêmes afin d'être certain que mon objet corresponde aux plus de modèles possibles. De même pour la position du projecteur qui est très variable, il vaut mieux ne pas entraver l'avant de l'objet.

Maintenant que les dimensions du **pico-projecteur** de base nous sont connues, il faut déterminer les principaux problèmes à régler par mon objet. En effet, ces pico-projecteurs n'ont pas de **transportabilité** inclue, j'entends par là que leur taille suffit à les transporter mais ils s'accompagnent le plus souvent d'un chargeur et même d'une télécommande, **les aérations** sont primordiales et doivent rester libres afin d'être certain de ne pas endommager le projecteur par la surchauffe, le **calibrage sur l'axe Z** est souvent très compliqué à mettre en place ou inexistante sur les modèles, **le son** encore de qualité inférieure mais qui peut être facilement réglé par l'intermédiaire d'une enceinte externe.

Il est alors important de régler les différents problèmes de la manière la plus simple possible par mon objet en se détachant de l'objet de référence:
- **le son** n'intervient pas dans mon objet car il fait appel à un autre accessoire déjà souvent compact et servant à d'autre utilisation que pour la projection vidéo.
- **l'aération** qui doit être maintenue libre
- **le calibrage sur l'axe Z**
- **le transport** à la fois du projecteur mais également de ses accessoires, obligatoires à son fonctionnement, c'est à dire télécommande et chargeur.

#### Calibrage sur l'axe Z

Pour régler le problème de calibrage, l'idée de la **bascule** semblait tout indiquait et toujours dans une recherche de simplicité, j'ai alors entrepris de réaliser différents modèles simples de bascules en cherchant tout de même à les maintenir et leur donner une stabilité et un "rail". Le procédé a marché assez facilement (à ma grande surprise) et j'ai, de là, pu effectuer des tests sur le **sens d'impression** afin de vérifier si ce dernier pouvait avoir un impact sur les forces de frottements s'exerçant entre les parties impimées. On remarque également une intention d'incorporer au socle les accessoires et ainsi les transporter en les incorporant à une partie necessaire.

![](images/base.png)

Le résultat a donc assez bien marché immédiatement et m'a donné le résultat souhaité. De plus, il s'avère qu'effectivement le **sens d'impression** joue un rôle dans la friction des parties en plastique et doit donc être pensé lors de l'impression.

![](images/impression_3D_bascule.png)

Il est alors venu l'hypothèse,étant donné qu'un système reposant sur le poid est assez aléatoire, de pouvoir cranter le mécanisme afin de pouvoir choisir un angle précis de visionnage et ainsi offrir une meilleure précision sur l'axe de rotation.
Cependant, après réflexion, il est alors semblé évident que dans ce cas là, la bascule n'avait elle plus vraiment d'intérêt une fois cranter et que le mieux était alors de la laisser telle qu'elle.
Cependant, des ajustements ont du être réalisé afin de permettre **le passage des fils jusqu'à une angle maximal de 30°** car en effet, si le pico-projecteur a besoin d'être relié à son chargeur, à un ordinateur ou même à une clé USB, sur sa face arrière, il faut alors laisser le champs libre au fil afn que ces derniers n'emcombrent pas le mouvement de **bascule**.

![](images/passage_fil_total.png)
![](images/arrière.png)

Après réflexion, pour des questions de sécurité lors du transport, il semble évident que la balance a besoin d'être crantée déjà pour offrir une sécurité comme dit précédemment, mais également afin d'offrir une meilleure précision pour l'angle de projection. Ces crans ont alors été fait tous les 15° et sur l'intégralité de la balance afin de véritablement bloquer la balance dans une position précise. Peut être que d'autres réglages seront nécessaires lors des tests afin de vérifier si des angles de 15° ne sont pas trop grands ou si les crans de la balance s'imbriquent bien dans l'ensemble de l'objet.

![](images/crans_final.png)

Et malheureusement non ! A mon grand désarroi, l'imprimante 3D qui ne m'avait jusqu'ici jamais laissé tomber ne m'a montré cette fois que des défauts dans ma construction. Peut être ai-je voulu faire trop "juste" pour les imbrications. Mais pour ce qui en est du crantage de la balance, j'ai pu observer un décalage assez important entre ma balance et mon support. Je me demandais donc d'où cela pouvait bien venir, si c'était **Fusion 360** qui imposait une marge d'erreur malgré que j'ai réalisé les deux formes en négatif. Mais après une conversation avec les fabmanageuses, il s'est avéré que cela pouvait venir de la mise à l'échelle réalisée sur le logiciel **PrusaSlicer** lui-même puisqu'en effet, j'ai exporté mes objets pour les imprimer séparemment et les ai ensuite réduits sur ce logiciel alors qu'elles m'ont expliqué que le mieux était en fait de réaliser la mise à l'échelle directement sur **Fusion 360** et de ne pas toucher à l'échelle sur **PrusaSlicer** car celle-ci n'est pas aussi précise. Je pense donc que le crantage est bon et peut fonctionner si cette idée est maintenue.

![](images/cran_essai_final.png)

Cependant force est de constater que malgré tout, l'emboîtement sur la partie supérieure de la balance, lui, fonctionne parfaitement est permet de bloquer le mouvement vers l'avant. 
Une nouvelle tentative suivant les conseils des fabmanageuses semble essentielle afin de vérifier la véracité du crantage.

![](images/impression_moitié.png)

Encore une fois ce maudit crantage ne marche pas à cause d'erreurs d'impression de la machine. En effet, il suffit d'un léger décalage afin que les pièces ne s'emboîtent pas et posent des problèmes. Ainsi, la prochaine tentative sur les conseils de la fabmanageuse sera plutôt de réaliser un crantage rectangulaire plutôt que pyramidal qui semble poser problème à la machine et également de réduire la longueur du crantage sur la balance afin d'être sûr que les erreurs d'impression n'interferont plus et feront fonctionner le prototype.

![](images/prototype_blanc_crantage.png)

#### Transport

Pour régler le problème du transport il fallait alors créer un objet permettant à la fois de transporter la bascule, le **pico-projecteur** et ses accessoires. J'ai alors eu l'envie d'essayer non pas en dessin où mes solutions sont le plus souvent trop complexes mais par la modélisation directe. Je suis ensuite parvenu à une forme permettant **la bascule**, en y incluant un receptacle pour les accessoires et permettant un transport de l'ensemble par une poignée supérieure. En effet, toujours dans cette recherche de simplicité, je me suis rendu compte en modélisant, au lieu de créer des systèmes de manettes se déployant ou avec des systèmes d'ouvertures ou de fermetures complexes que le plus simple était tout bêtement de faire une poignée rentrant dans le design de l'objet et offrant un transport simple sans gâcher les qualités esthétiques.

![](images/essai_balance.png)

Il y a là une volonté de placer les accesssoires du **pico-projecteur** dans **la bascule** elle-même tout en y mettant un sens afin de bien répartir le **poids de la bascule**. Il a ensuite s'agit de placer cette forme sur le plateau de l'imprimante 3D afin, comme expliqué ci-dessus, de prévoir un bon **sens d'impression**.

![](images/impression_orange.png)

On remarque alors aussi que les problèmes **d'aération** sont réglés dans la mesure où il n'y a plus de "caches" du projecteur et que ce dernier a alors l'espace ouvert suffisant pour évacuer la chaleur.
Elément encore précoce mais essentiel qu'il est important de souligner dès maintenant est la finition de l'objet qui est loin d'être parfaite à cause des supports qui sont, lors de cette impression, bien trop proches et collés à la forme de l'objet. Il est prématuré de s'occuper de cela mais autant le remarquer et savoir comment le corriger maintenant afin de ne pas perdre un temps précieux plus tard. J'ai ainsi lancé une autre impression une fois les réglages des **supports** modifiés afin d'observer la différence de rendu.

![](images/finitions.png)

Cependant ce modèle de prise pour le transport peut être modifié et amélioré sans aucun doute car un système de cran peut être installé pour l'axe de rotation, certes, mais plusieurs questions se posent à savoir **le passage des fils** par exemple si l'on souhaite brancher l'ordinateur. En effet, dans ce cas par où passe les fils et comment ne vont-ils pas gêner le mouvement du balancier ? Il y a également le problème du rayon de lumière du projecteur qui peut être obstrué par l'objet. Ainsi faut-il peut-être l'élargir au sommet pour qu'il ne gêne rien tout en se méfiant des capacités maximales d'impression de **l'impression 3D**.

Afin de régler le problème de transport, il a suffit de remplacer la poignée que j'avais alors réalisé et qui était, certes esthéthique, mais peu utile par une poignée simple mais aux mêmes dimensions finales que celles du modèle d'essai qui offre des qualités ergonomiques. En effet, la prise est simple et permet de bien placer la main pour une saisie sûre et franche.

![](images/poignée_totale.png)
![](images/poignée_fin.png)

#### Socle

La mise en place d'un socle semblait nécessaire dans la mesure où le poids semblait mal réparti et la stratégie de placer **le chargeur** dans la bascule ne s'est pas avérée fructeuse puisque cela gênait la précision de la bascule. il a fallu alors ajouter de la matière et touver dans cet espace de conception déjà restreint par les dimensions d'impression de la machine un moyen de placer le **chargeur** afin que ce dernier soit accessible tout en ne gênant pas l'ensemble de l'objet. Il a fallu alors modifier légéremment la forme et placer une cale afin d'empêcher l'objet de basculer vers l'avant. Cependant, la réalisation du prototype sera essentielle afin de vérifier si les dimensions d'entrée pour **le chargeur** sont bonnes car j'émets de gros doutes sur ces dernieres. Le prototypage devrait nous fournir ces informations rapidement.

![](images/chargeur_totale.png)
![](images/rangements.png)

Effectivement, mes craintes se confirment dans la mesure où après réalisation du prototype, on se rend compte très rapidement que l'espace réservé aux fils et bien trop petit et ne permettrait donc pas de stocker les branchements que ce soit pour charger l'appareil ou pour le relier à l'ordinateur. L'idée est alors de peut-être **rajouter une pièce supplémentaire** qui ferait alors office de socle tout en amenant un rangement pour les branchements. Mais cette dernière pièce doit pouvoir s'imbriquer avec le premier objet afin de laisser paraître une certaine **unité** dans l'ensemble. On doit donc concevoir une nouvelle pièce qui parviendrait à se relier suffisamment au premier objet pour ne pas tomber lors du transport de l'ensemble mais qui puisse tout de même s'enlever afin de pouvoir se servir des branchements même lorsque la projection a commencé et bien sûr, sans gêner cette dernière. Je suis donc parti sur une forme **complémentaire** à mon premier objet et qui permettrait l'imbrication de ce dernier sur lui grâce à un système de **rail** offrant ainsi un maintien suffisant pour le transport.

![](images/socle_final.png)

Le prototypage m'a permis de mettre en évidence deux gros problèmes dans mon modèle. Le premier est l'impossibilité de fabriquer des **rails** aussi fins. Malheuresueemnt je n'ai pas osé aller plus loin lors de la création de mon modèle alors que je le peux parfaitement et cela est même obligatoire afin d'avoir des résultats car sinon les constructions en plastiques se cassent tellement elles sont fines et en plus selon le sens d'impression, elles risquent de ne pas être précises. Il faut également rappeler que ces rails vont être soumis à des forces importantes dans la mesure où elles permettent un glissement mais également de maintenir le socle au support. Il faut donc qu'elles soient non seulement bien réalisées mais aussi robustes d'où l'épaisseur supplémentaire à ajouter. Le second problème du socle est sa fragilité. en effet, voulant toujours plus d'espace pour les chargeurs et autres fils électriques, j'ai réduit les épaisseurs à leurs minimums et cela fragilise considérablement l'objet qui du coup, surtout avec le sens d'impression mis en place, se casse dès la moindre pression. Il va de soit que mon objet doit être solide et cette étape ne demande que quelques ajustements.

![](images/rail_final.png )

Après réflexion, il a alors été décidé de ne pas créer de socle qui soit dissocié du support principal pour des problèmes de mise en place avec le système de rail qui demande un ajustement bien trop conséquent mais également pour une raison esthétique dans la mesure où ce dernier alourdit l'ensemble de l'objet. L'idée a alors été lancé de placer les fils du chargeur, et reliant le pico-projecteur à l'ordinateur ainsi que la télécommande, dans un réceptacle, ou deux, placé(s) sur les côtés du support principal. Cela permettrait donc le transport de l'intégralité des objets et d'avoir le socle et le support le tout en une seule pièce. Au point de vue ergonomie, cela est de toute façon plus simple pour l'utilisateur qui peut alors sortir et rentrer les fils ou la télécommande même pendant la projection. De plus, cela est plus simple de saisir les objets par une ouverture vers le haut que par le bas où l'utilisation est plus difficile. 

#### Objet à part entière

Mon développement s'est en réalité divisé en différentes phases puisqu'au début de la réalisation de l'objet, lors des recherches de formes, je réalisais par le dessin de nombreux croquis qui donnaient un objet sculptural, certes, mais qui ne fonctionnait pas. Pour palier à ce problème, j'ai ensuite décidé de d'abord réaliser un objet **fonctionnel** afin de bien comprendre les attentes du pico-projecteur mais également les limites des outils numériques présents au fablab et de façonner un véritable objet car ce dernier doit être fonctionnel dans le sens où il ne s'agit pas ici d'art mais bien de design. Le design impose donc d'avoir une utilité et une fonctionnalité certaine. Mon objectif était donc de m'intéresser à la **fonctionnalité** pour ensuite m'intéresser à **l'esthétisme** pour finir par l'aspect **paramétrique** que peut prendre mon objet. A présent que mon objet fonctionne pour la plupart des besoins du pico-projecteur, il est maintenant temps de se questionner sur l'esthétisme de l'objet qui doit, en dehors de sa fonction, avoir tout de même une vie propre et être un objet à part entière. Pour cela, j'ai essayé une réponse élégante sur le profil de l'objet et son imbrication avec son socle afin de marquer une continuité entre les deux parties à l'image de mon objet de référence qui cherche un certain lissage et une pureté des formes. J'ai également réfléchi à une possiblité de fermer les bords afin d'amener une meilleure protection pour l'objet et de l'extirper de sa condition **d'accessoire** pour en faire un objet véritable. Ce principe marche sur un coulissement des bords dans un interstice sur le côté de l'objet. On peut ensuite le saisir et le changer facilement grâce à une **saisie** placée sur la poignée de transport.

![](images/bords_final.png)

Cela pose de gros problèmes de construction. En effet, il faut d'abord voir si l'idée est valide avant de se lancer dans des tests supplémentaires car elle impose beaucoup de contraintes. En effet, il y a tout d'abord la finesse du bord de l'objet qui peut poser certains problèmes lors de l'obtention de la pièce car cette dernière est remplie de supports et le moindre obstace empêche le coulissage fluide du **cache** qui peut ainsi se casser à son point le plus faible (comme cela m'est arrivé d'ailleurs). Pour pallier à ce problème, peut-être qu'un simple **Congé** à cet endroit là permettrait de minimiser les risques de casse. De plus, le **cache** ne peut pas intégralement rentrer dans sa place car le fond est rempli de support. il faut donc prévoir un autre sens d'impression ou bien un moyen afin d'extirper les supports afin de laisser le passage libre. Par le bas peut être ? Et finalement le dernier problème qui est surement le moindre est la cassure de la branche du support par un trop de précision de mon modèle sur les côtés où une marge d'erreur doit être laissée afin de ne pas trop contraindre le plastique et ainsi éviter qu'il ne se brise.

![](images/bords_essai.png)

Il y a également une possibilité de transformer l'objet en quelque chose d'autre. En effet, lors d'une correction il a été émis l'idée de transformer cet objet en lampe lorsqu'il ne fonctionne pas. J'ai préféré alors faire quelques essais afin de me rendre compte de ce que le résutat pourait donner avant de me questionner sur comment rendre une telle idée possible.

![](images/lampe_final.png)

Mais l'objet peut aussi avoir une identité propre du moment qu'il se suffit à lui-même. Autrement dit, l'objet, s'il est suffisamment élégant, peut tout à fait convenir et devenir un objet design posé sur une table ou sur une étagère sans pour autant devoir devenir autre chose. Il faut alors trouver une réponse élégante qui permette de rendre l'objet suffisamment design pour être exposé ou du moins laissé dehors.
Ainsi, j'ai abandonné les coulissages de côté qui manifestement ne pouvaient être réalisés simplement avec l'impression 3D pour conserver simplement la forme de base élargie par les rangements des cables et de la télécommande. Une réponse simple en somme.

Une autre difficulté a fait son apparition lors de ce dernier jury, il a fallu modifier le modèle car ce dernier n'était pas réalisable par les imprimantes 3D **Prusa**. En effet, étant parti du postulat que les impriantes avaient une capacité d'impression de **25x25x30**, j'ai été surpris d'apprendre qu'elles ne peuvent en réalité, imprimer dans des dimensions aussi grandes puisqu'elles impriment pour des mesures de 21x21x25. Il a fallu ainsi modifier le modèle lors de ces derniers ajustements afin d'être certain de pouvoir obtenir l'objet avec le matéreil du fablab et cela en une fois. Ainsi les données sont respectées afin que l'objet rentre tout juste dans les dimensions d'impression de l'imprimante avec la possibilité de placer tout de même tout types de pico-projecteurs.

![](images/pers_final_2.png)

Il ne reste donc plus qu'à tester si l'impression pourrait fonctionner à la suite de ces modifications et effectivement d'après **PrusaSlicer**, il n'y aurait pas de problème à cela. Il faudra tout de même tester afin d'être sûr de ne pas avoir de mauvaises surprises par la suite.

![](images/impression_finale.png)

Sur le dernier prototype à l'échelle 1/2 réalisé, les problèmes de crantage persistent encore du fait des erreurs que la machine peut produire. Des mesures ont donc été prises afin que ces derniers n'interfèrent plus et que du coup chacun puisse réaliser ce support à pico-projecteur sans se soucier des erreurs de la machine. De plus, durant cette impression de plus de 23h, il est important de constater que la buse doit être préalablement nettoyée à l'aide d'acétone car sinon des résidus marron qui sont en réalité des restes de plastique cramés peuvent s'immiscer dans l'impression et gâcher le rendu final comme ce fut le cas pour mon prototype à l'échelle 1/2. On remarque également une malfaçon au niveau de l'angle montant que la machine est sensée pouvoir réaliser sans avoir besoin de support ce qui signifie que les support à cette endroit gâchent un rendu qui peut se faire sans leur aide. Il faut donc ajuster cela pour que l'objet n'est plus aucun défauts et soit aussi esthétique que pratique. Mais on sent bien ici qu'on est au stade des finitions de l'objet et que ce dernier est bientôt prêt et n'a besoin que d'ajustements.

![](images/prototype_blanc.png)
![](images/prototype_blanc_défauts.png)

### Finalité

Il a été décidé de finalement retourner en arrière afin de retrouver l'esthétisme "léger" que fournissait au préalable l'objet. Mais le rangement pour la télécommande ainsi que pour les fils restait tout de même un problème. Il a ainsi été décidé de faire un socle indépendant sur lequel reposerait le porte-projecteur lorsqu'il ne sert pas. Il serait ainsi mis en valeur sur un socle une fois posé sur ce dernier et ce socle pourrait devenir un objet à part entière lorsque le porte-projecteur n'est pas là. On a donc deux objets distincts et indépendants qui ne s'attachent pas mais peuvent tout aussi bien être rangés qu'exposés.

Il a donc fallu penser ce socle et l'effet qu'il aura sur l'ensemble.

![](images/socle_finalité_essai_1.png)
![](images/socle_finalité_essai_2.png)

**COVID** oblige, une fois mon vol annulé et les circonstances se dégradant en France, je me suis retrouvé bloqué en France sans possibilité de pouvoir imprimer en 3D au Fablab. Par chance, j'ai moi-même une vieille imprimante 3D datant de 2014 qu'il a fallu que je répare (et cela pendant plusieurs jours...). De plus, il a été nécessaire de modifier mon modèle 3D afin qu'il s'adapte au mieux à mon imprimante qui n'est capable d'imprimer que dans les dimensions **20x20x20**. Cependant, une chose est sûre, si mon imprimante peut imprimer mon modèle, toutes les autres imprimantes le pourront sans difficultés ce qui garantit alors un modèle productible par n'importe qui. 
Le paramétrage a néanmoins dût être abandonné à cause du manque de réponses des producteurs sur leurs modèles et également la réparation et utilisation de mon imprimante qui reste tout de même très chronophage étant donné l'ancienneté de la machine.

J'ai réussi, malgré cela, à réaliser un modèle compatible qu'il ne me reste plus qu'à réaliser à taille réelle.

#### Théorique

Voici quelques mises en scène du projet finalisé et de son utilisation.

![](images/séquence_1.png)
![](images/esquisse_2.png)
![](images/esquisse_final.png)

#### Pratique

Dans la pratique, la réalisation du modèle fût assez compliqué avec de nombreux essais infructueux qui venait tout d'abord de la machine à laquelle il fallait remplacer certaines pièces et puis finalement à l'extrudeur lui même qui devait être resserré en passant par la chaleur de la buse qui devait être augmentée pour atteindre les 235 degrés (ce qui ne dépend en réalité que du fil). 

![](images/balance_infructueux.png)

Cela a donc occasionné de nombreux ratés mais avec des ajustements, il a finalement été possible de réaliser des pièces satisfaisantes et fonctionnelles.

![](images/réussite.png)
![](images/avancée.png)

Pour des raisons pratiques et surtout pour des questions de rendu, il a été décidé de couper l'armature en son centre afin de l'imprimer en deux fois pour ensuite le coller. De cette manière, l'utilisation de supports devient inutile est le rendu est plus nette sans les résidus de ces dits supports.

![](images/découpe.png)

### Notice

La fabrication de l'objet est simple mais il est tout de même important de la rappeler par une axonométrie efficace. Ainsi, les pièces s'assemble pour former deux entités que l'on peut ensuite fixer grâce à des aimants. 

![](images/axonométrie.png)
