Salut! Je m'appelle **Fabien**

Je vous laisse découvrir au fil de cette page qui je suis, mes différents projets et l'avancement de mes recherches en Design !


## A mon sujet

![](images/_profil_2.png)


Originaire de Nice, une ville du sud de la France, j'ai obtenu un bachelier de **Design d'Espace** dans une école avant de continuer dans cette voie qui a toujours été une passion pour moi: l'Architecture.

Mon choix s'est rapidement porté sur la **Faculté d'Architecture de La Cambre-Horta** qui me permettait non seulement d'avoir un apprentissage des plus complet et des plus renommé, mais également de partir dans un autre pays afin de forger mon caractère. 

A présent en MA2 dans cette institution, j'ai décidé de participer à la question d'architecture "**Architecture et Design**" afin d'approfondir mes connaissances en Design et me familiariser avec les outils digitaux qui forment, aujoud'hui, une mine de possibilités et de créations dans le domaine de notre profession future.

## Centres d'intérêt et passions

Le dessin a toujours fait partie intégrante de ma vie et j'essaie de l'utiliser au maximum dans mes études d'architecture. C'est d'ailleurs sur le dessin que porte mon mémoire et la place qu'il occupe pour l'architecte contemporain. 
Sinon la musique et le sport sont des éléments vitaux pour moi.

## Parcours

La plupart de mes projets sont donc des tentatives de mélanger mes deux cursus où j'essaie d'allier **Architecture** et **Design** avec le plus de simplicité possible.

### Projet A

Pour un projet, il m'a été demandé de penser un espace sous un observatoire, oeuvre de **Gustave Eiffel** et **Charles Garnier**, ce dernier se transforme alors sous la salle même de l'observatoire en une promenade méditative visant à élargir la pensée de l'astronome afin de le faire passer de son échelle humaine à l'échelle plus large des phénomènes qu'il étudie.

![](images/compo.png)

### Projet B

Un autre projet consistait en l'aménagement d'une maison de village en kot étudiant. La difficulté de ce dernier consistait à maximiser l'espace pourtant extrêmement restreint et à faire des espaces qualitatifs par les ouvertures, seuls moyens d'augmenter l'espace et de créer des respirations visuelles. Ce projet m'a permis de m'exprimer aussi bien en terme de 3D que de productions graphiques 2D.

![](images/pop_up.png)

### Rapport au digital

Ma première expérience digitale (et je n'inclue pas ici les débuts sur les software d'architecture qu'il est obligatoire d'apprendre comme _Autocad_ ou _Sketchup_) remonte en réalité à il y a 4 ou 5 ans lorsqu'un ami à moi en Architecture à l'école de Marseille m'a pour la première fois parlé des impressions 3D qu'il souhaitait réaliser afin de gagner du temps sur ses maquettes. J'entamais alors ma dernière année de Design d'Espace et il m'a convaincu qu'une imprimante 3D pourrait m'aider à développer mes perspectives de création.
Ainsi, j'ai dépensé mon salaire de l'été dans une imprimante 3D de type _RepRap_ que j'ai reçu en kit et que j'ai alors assemblé moi-même en dehors du circuit électronique qu'il fallait juste paramétrer très simplement. Malheureusement, il s'agissait encore des debuts de la démocratisation de cet outil et les problèmes sont vite apparus. Après quelques réparations, mes compétences n'étaient pas assez complètes pour pouvoir régler les défauts restants. De plus, les pièces même de l'imprimante ne resistaient pas très bien aux forces s'exerçant sur elles et il faillait souvent les remplacer.

#### 2018 - 2019

En BA3, j'ai eu l'ocassion de pouvoir intégrer l'atelier de **Digital Fabrication Studio** de Q2 où j'ai pu en apprendre plus sur les imprimantes 3D ainsi que sur les softwares permettant de les faire fonctionner, et sur les découpes laser qui m'ont particulièrement intéressées notamment en terme de gravure. Lors de cette année, j'ai réalisé une recherche sur les fractales et leur impacts sur le monde qui nous entoure afin de voir si une application théorique était possible en architecture. En effet, la recherche de théorie permettant de réfléchir à un espace parfait n'est pas nouveau et je voulais comprendre comment cette idée pouvait être développée.

![](images/fractales.png)

#### 2019 - 2020

En MA1, toujours lors du second quadrimestre, je suis retourné dans ce même atelier afin, cette fois, de m'intéresser aux softwares permettant d'améliorer ou d'aider le travail de l'architecte. Ce travail en rapport avec mon mémoire m'a permis de survoler différents aspects du numérique en architecture avec le paramétrique, l'intelligence artificielle... et de voir les différents moments où la conception ne passe plus par la main de l'architecte avec le dessin mais directement par l'informatique.

![](images/outils_numériques.png)


## Mon objet


Notre projet pour ce quadrimestre dans la question d'architecture "**Architecture et Design**" est de transformer un **objet design emblématique des années 60-70 et de le ramener dans notre société contemporaine**. Ces années marquent les débuts du plastique dans le domaine de la réalisation d'objets du quotidien et sont une source d'inspiration et de documentation importantes sur cette époque.  

C'est grâce à l'intermédiaire de l'exposition **Plasticarium** qu'il nous a été permis de voir et appréhender ces objets particuliers et pour la plupart extrêmement ingénieux. Comme le souhaite l'investigateur de cette exposition **Philippe Decelle**, il s'agit, à travers ce musée et ces différents objets, de changer le point de vue des gens sur un matériau, qui, dorénavant, est méprisé, en comprenant qu'il n'en a pas toujours été ainsi. En effet, dans les raisonnements actuels, le plastique acquiert un caractère péjoratif dans la mesure où nous sommes dans une société de plus en plus enclin aux démarches écologiques. Mais ce plastique a, à une époque, offert de grandes possibilités de création et rendu possible des gestes et inspirations jusqu'alors impensables. C'est dans cette optique que **Philippe Decelle** a collectionné des meubles et objets design des années "plastiques" et nous offrent aujourd'hui une collection impressionnantes d'objet tous aussi surprenants les uns que les autres et pourtant remarquablement bien pensés ayant profondément changé la manière de vivre et de penser du foyer moyen de leurs époques.

![](images/visite_musée.png)


### Choix de l'objet


#### Description

**Tondo Junior CT1** est un jouet pour enfant permettant à ces derniers de pouvoir projeter leurs propres films par l'utilisation de bobines. L'objet en lui-même se compose d'un socle plus haut que large de couleur orange permettant les branchements à l'arrière et les réglages sur les côtés, sur lequel repose une sphère blanche séparée en son centre par une bande de même couleur que le socle. La sphère supérieure est donc en deux partie : une partie contient les moteurs servant à faire tourner la bobine et certainement le circuit électrique, tandis que l'autre contient le projecteur et les roues où la bobine doit être placée. Il n'y a que cette dernière partie qui soit accessible, l'autre étant complétement bloquée. Cet objet ne laisse rien soupconné de sa fonction lorsqu'il est fermé. Il ne ressemble qu'à une sphère posée sur un socle, séparée en son centre par une ligne orange. Et là, l'objet fait preuve, pour l'époque, d'une grande sobriété et est donc déjà très contemporain avant l'heure. Mais lorsqu'on l'ouvre, on se rend compte alors, avec surprise, du réel potentiel de l'objet et de ce qu'il permet: amener une salle de cinéma dans n'importe quel foyer. 

#### Raisons de ce choix

Cet objet particulier me semble tout droit sorti d'un _Star Wars_ ou du film _2001, L'Odyssée de l'Espace_ et m'a donc tout de suite plu. Hormis son air lointain de _BB-8_, j'ai pu rapidement imaginer le bruit d'une bobine de film passant dans ce mécanisme et donnant ainsi une sensation toute particulière de pouvoir se projeter dans une autre époque. Même effet qu'offre d'ailleurs la pointe en diamant d'un tourne disque sur le vinyl. Il nous donne l'impression, durant un bref instant, d' entrevoir ces époques qu'ont vécu nos parents dans leur jeunesse et nous lie en quelque sorte au passé. Bien qu'il ne s'agisse pas de notre époque, certains objets ont cet effet étrange à la manière de la _madeleine de Proust_ de nous renvoyer des images du passé ou même à des souvenirs racontés par nos parents. Quelle sensation cela devait être de mettre une bobine et de pouvoir projeter un film chez soi où qu'on le veuille. 
Son design m'a également interpellé dans la mesure où les années 60-70 ne sont pas, pour moi, mes années préférées en termes de design. En effet, malgré que ces dernières recèlent de véritables merveilles, beaucoup me repoussent par leur extravagance de forme ou de couleur et cet objet, assez épuré mais d'une grande complexité interne a immédiatement attiré mon regard. A bien y réfléchir, lorsque je regarde l'intégralité des photos prises lors de cette visite au musée, on remarque clairement que cet objet m'a tout de suite plu car c'est le seul objet que je photographie en mode portrait afin de le séparer des autres, de son contexte temporelle pour ne garder que lui et le mettre en avant. Sans doute déjà, une décision inconsciente de ma part.



![](images/choix_objet.png)

**G.L. CERNUSCHI**

_**Tondo Junior CT1**_

_Projecteur en ABS_

_1969_
