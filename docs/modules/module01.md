# 1. Etat de l'art et documentation

Notre projet pour ce quadrimestre dans la question d'architecture "**Architecture et Design**" est de transformer un **objet design emblématique des années 60-70 et de le ramener dans notre société contemporaine**. Ces années marquent les débuts du plastique dans le domaine de la réalisation d'objets du quotidien et sont une source d'inspiration et de documentation importantes sur cette époque.  

C'est grâce à l'intermédiaire de l'exposition **Plasticarium** qu'il nous a été permis de voir et appréhender ces objets particuliers et pour la plupart extrêmement ingénieux. Comme le souhaite l'investigateur de cette exposition **Philippe Decelle**, il s'agit, à travers ce musée et ces différents objets, de changer le point de vue des gens sur un matériau, qui, dorénavant, est méprisé, en comprenant qu'il n'en a pas toujours été ainsi. En effet, dans les raisonnements actuels, le plastique acquiert un caractère péjoratif dans la mesure où nous sommes dans une société de plus en plus enclin aux démarches écologiques. Mais ce plastique a, à une époque, offert de grandes possibilités de création et rendu possible des gestes et inspirations jusqu'alors impensables. C'est dans cette optique que **Philippe Decelle** a collectionné des meubles et objets design des années "plastiques" et nous offrent aujourd'hui une collection impressionnantes d'objet tous aussi surprenants les uns que les autres et pourtant remarquablement bien pensés ayant profondément changé la manière de vivre et de penser du foyer moyen de leurs époques.

Malheureusement, ayant été absent la première semaine de cours pour des raisons de confinement, je n'ai pu choisir que plus tard mon objet mais cette exposition est un véritable retour dans le passé qui nous permet de comprendre l'évolution du design et l'état d'esprit qui à façonner l'état d'esprit de nos parents. 

![](../images/visite_musée.png)

## Relation à l'objet

**Tondo Junior CT1** est un jouet pour enfant permettant à ces derniers de pouvoir projeter leurs propres films par l'utilisation de bobines. L'objet en lui-même se compose d'un socle plus haut que large de couleur orange permettant les branchements à l'arrière et les réglages sur les côtés, sur lequel repose une sphère blanche séparée en son centre par une bande de même couleur que le socle. La sphère supérieure est donc en deux partie : une partie contient les moteurs servant à faire tourner la bobine et certainement le circuit électrique, tandis que l'autre contient le projecteur et les roues où la bobine doit être placée. Il n'y a que cette dernière partie qui soit accessible, l'autre étant complétement bloquée. Cet objet ne laisse rien soupconné de sa fonction lorsqu'il est fermé. Il ne ressemble qu'à une sphère posée sur un socle, séparée en son centre par une ligne orange. Et là, l'objet fait preuve, pour l'époque, d'une grande sobriété et est donc déjà très contemporain avant l'heure. Mais lorsqu'on l'ouvre, on se rend compte alors, avec surprise, du réel potentiel de l'objet et de ce qu'il permet: amener une salle de cinéma dans n'importe quel foyer. 

![](../images/choix_objet.png)

**G.L. CERNUSCHI**

_**Tondo Junior CT1**_

_Projecteur en ABS_

_1969_

Cet objet particulier me semble tout droit sorti d'un _Star Wars_ ou du film _2001, L'Odyssée de l'Espace_ et m'a donc tout de suite plu. Hormis son air lointain de _BB-8_, j'ai pu rapidement imaginer le bruit d'une bobine de film passant dans ce mécanisme et donnant ainsi une sensation toute particulière de pouvoir se projeter dans une autre époque. Même effet qu'offre d'ailleurs la pointe en diamant d'un tourne disque sur le vinyl. Il nous donne l'impression, durant un bref instant, d' entrevoir ces époques qu'ont vécu nos parents dans leur jeunesse et nous lie en quelque sorte au passé. Bien qu'il ne s'agisse pas de notre époque, certains objets ont cet effet étrange à la manière de la _madeleine de Proust_ de nous renvoyer des images du passé ou même à des souvenirs racontés par nos parents. Quelle sensation cela devait être de mettre une bobine et de pouvoir projeter un film chez soi où qu'on le veuille. 
Son design m'a également interpellé dans la mesure où les années 60-70 ne sont pas, pour moi, mes années préférées en termes de design. En effet, malgré que ces dernières recèlent de véritables merveilles, beaucoup me repoussent par leur extravagance de forme ou de couleur et cet objet, assez épuré mais d'une grande complexité interne a immédiatement attiré mon regard.

## Apprentissage du Module 1

Il nous a été demandé de documenter l'intégralité de nos recherches et de nos travaux afin que ces derniers soient non seulement toujours accessibles par nous mais également par le reste de l'atelier.

Pour ce qui est de changer et créer un texte sur **GITLAB**, cela ne pose pas de véritable soucis mais il est vrai que **Markdown** possède quelques défauts comme par exemple de ne pas assurer certaines fonctions de mises en pages auxquelles nous sommes habitués avec des logiciels tels que **Indesign** ou même **Word** (citons par exemple les écarts entre les paragraphes qui sont parfois trop marqués, les alinéas que l'on ne peut pas réaliser,...). La seule solution pour pallier à ces problèmes semble être de passer par l'intermédiaire du **HTML** et je trouve cela dommage dans la présentation de nos travaux mais également parce que le **HTML** m'est ecore plus étranger. Un autre problème pour moi et la police d'encodage qui m'empêche de bien me relire et où, du coup, je laisse passer des fautes d'orthographes abominables! Mais après, cela doit être l'écriture standard pour l'encodage, je ne peux pas m'exprimer sur ce sujet vu ma complète ignorance dans ce domaine.

Hormis cela, je n'ai pas encore touché à **Atom** ou aux manipulations qu'il est possible de réaliser directement via l'outil _Terminal_ de Mac malgré qu'on m'ait prouvé que cela permettait un gain de temps considérable. Mais je préfère me cantonner pour le moment aux modifications en direct via l'**Edition** avec **Markdown** et il faut également avouer que l'intallation de GIT pour s'en servir via le terminal n'est pas une chose si facile.
J'ai cependant remarqué autre chose, lorsque j'importe une image sur le site, sa taille semble être de toute manière réduite. En effet, une image qui fait sur **Photoshop** 500Kb, une fois passée dans le dossier _Imanges_ apparaît ne faisant plus que 0,3 Mb soit 300Kb. Ce peut-il que **GITLAB** compresse automatiquement nos fichiers ? Si tel est le cas, je n'ai remarqué aucune perte de qualité dans les photos. 
