# 2. Conception assistée par ordinateur

La première étape a été alors, afin d'appréhender aussi bien le logiciel de rendu 3D qui est **Fusion 360** que l'objet lui-même de tenter une modélisation 3D. Et cet exercice permet déjà une première analyse et une première approche intéressante en nous obligeant à regarder l'objet plus en détails avec ses jonctions, ses mouvements de rotation, ses systèmes d'ouverture et de fermeture...

Pour avoir réalisé deux objets durant ce module. Je peux assurer que les plus petits objets sont parfois les plus complexes dans la mesure où ils recèlent une multitude de détails qu'il faut tout de même modéliser afin de comprendre l'objet dans son esnemble. Avec le _**Tondo Junior CT1**_ par exemple, l'utilisation d'un assemblage n'a pas été nécessaires mais la création de petites pièces, demandant sans cesse des points de reférences, de translation ou des axes de rotation a véritablement été la plus grosse partie de cette modélisation.

## Etape 1

La première partie a donc consistait à créer les bases soit une sphère posée sur un socle avec ce socle se pliant parfaitement à la forme de la sphère.
Pour ce faire, la première chose est de réaliser une **Esquisse** suivie ensuite part une **Révolution** autour d'un axe dans le but d'obtenir une sphère. Cela étant fait, on réitère l'opération mais cette fois pour créer le socle et cela avec l'outil **Extrusion**. Une fois nos deux objets obtenus et emboîtés, il faut s'ocupper de séparer les différentes parties de l'objet. En effet, certaines parties sont blanches et d'autres oranges. De plus, le support du rétroprojecteur est orange et coupe une sphère. Pour ce faire, on crée alors un plan en décalé où l'on va venir dessiner la forme qui nous intéresse pour ensuite venir l'extruder afin qu'elle coupe la sphère nous donnant ainsi la bonne forme. On enchaine ensuite avec la bande centre orange où, le la même manière, on vient créer une **Esquisse** pour ensuite faire une **Extrusion** mais cette fois en demandant au logiciel de créer un **Nouveau corps**. Ainsi le corps est entier et indépendant. afin de séparer les deux parties de la sphère, oragne et blanche, il faut réaliser la commande **Scinder** qui va nous permettre de couper notre sphère en différents morceaux en se servant du nouveau corps créé comme moyen de coupe. Il vous siffit par la suite de supprimer les corps inutiles et d'**Associer** ceux qui vous intéressent. Pour moi, dans ce cas les parties oranges.

![](../images/étape_1.png)

## Etape 2

La prochaine étape était ensuite de réaliser le projecteur lui-même reposant sur la face plate orange. Pour cette forme particulière on utilise d'abord une **Esquisse** avant de créer une autre forme plus en hauteur sur un plan parallèle dans le but d'utiliser la fonction **Lissage** permettat de relier deux formes différentes. On utilise ensuite des **Congés pour arrondir la forme et la rendre au plus près de la réalité possible. Ensuite, il faut créer l'arrondi à l'arrière de ce projecteur qui va laisser la place à une bobine de tourner. bien que le corps du projecteur soit réalisé, il manque tout de même la protubérance de laquelle sort l'objectif avec la lentille. Pour cela, il faut **Construire un nouveau plan**, créer une **Esquisse** et enfin **Extruder** cette esquisse. On utilise ensuite le même procédé pour la focale et la lentille devant être créer à l'intérieur du projecteur et sortir par l'ouverture de la protubérance. pour donner plus de réalisme, j'ai donné une **Apparence** émissive à la lentille. Ainsi, les textures émissives se trouvent dans le fichers **Other** et **Emissive**. On peut ensuite modifier les différents paramètres d'un rendu afin de le rendre le plus proche de la réalité possible. En effet, la lumière de base pour la lumière est blanche et beaucoup trop puissante. C'est alors qu'en allant dans les paramètres du matériau j'ai pu modifier les **Lumen** ainsi que la **Couleur**.

![](../images/étape_2.png)

## Etape 3

Il faut, à présent que nous avons notre base d'objet, s'intéresser à ses détails. Pour ce faire il faut donc modéliser pour l'étape suivante les embouts permettant aux bobines de tourner. Pour cela, il faut toujours suivre le même procédé en prenant toujours bien soin de créer les nouveaux calques suivant la bonnes références. Le reste et une succession d'**Esquisses** et d'**Extrusions** permettant peu à peu de faire sortir la forme. Toujours utilisé à la fin des **Congés** ou **Chanfreins** afin de peaufiner au mieux la forme. A la fin, un copier coller des différent corps permet ainsi de replacer le même objet à un autre endroit. Pour cela, j'ai personnellement utilisé le déplacement **Point à point** qui contraint ainsi à soigneusement choisir le plan et où placer le point de référence. C'est la manière de faire qui me semblait la plus simple et claire même si je dois avouer que cette technique peut se révéler parfois très compliquée avec une mutliplication de points de référence qui restent présent tout au long de la modélisation et peuvent créer des erreurs comme j'ai pu en avoir. 

![](../images/étape_3.png)

## Etape 4

La suite logique est alors de s'intéresser aux bobines elles-mêmes. Pour ce faire, on se sert des embouts précédemment modélisé afin de créer des **Plans de décalage** sur lesquels on va faire nos **Esquisses**. En l'occurence, il s'agit des cercles des bobines. On fait donc le premier avant de l'**Extruder** pour ensuite redessiner une **Esquisse** sur cette nouvelle face et **Extruder** à nouveau le résultat. On réalise ensuite, avant de répéter à nouveau l'opération dans le but de créer l'autre grand cercle qui forme la fin de la bobine, le motif présent sur la face de la bobine qui se présente comme des ouvertures. Pour ce faire, il faut **Créer un axe** grâce à deux points. Ensuite on réalise notre motif en le dessinant sur une **Esquisse**  et en l'extrudant par la suite. On utilise alors ensuite la fonction **Réseau circulaire** qui peut nous permettre de sélectionner soit un tracé sur une **Esquisse**, soit, dans le cas qui nous intéresse, une fonction. On choisi donc la fonction **Extruder** qui nous a permis de créer le premier motif pour ensuite le faire trouner sur l'axe que l'on a créé précédemment. Ainsi le motif se répète autour de l'axe. On fait alors le même procédé sur la dernière face extruder et on réalise même le motif au centre de la bobine par une suite d'**Esquisse**, **Extrusion** pour couper cette fois, et enfin des **Congés** comme on les utilise tout au long de la modélisation afin de coller à la réalité et d'assouplir la 3D. Pour finir, un copier-coller de la première bobine et un **Déplacement** point à point permet de se retrouver avec les deux bobines placées aux bons endroits.
Un élément important dans ce genre de conception est de faire attention lorsque l'on extrude ou crée une forme, de s'assurer si l'on coupe, joint ou crée un nouveau corps. Cela permet ainsi de ne pase se perdre dans la modélisation et d'avoir de mauvaises surprises.

![](../images/étapes_4.png)

## Etape 5

la prochaine étape est de réaliser les encoches par lesqelles la partie de la demi-sphère blanche se bloque sur la partie orange. En observant l'objet, on se rend compte que l'ouverture est au dessus et que 3 points différents sont creusés pour permettre de bloquer la partie en question. Enfin, afin d'améliorer le coté esthétique, les bord sont creusé afin que l'imbrication se fasse mieux et ne laisse aucune trace visuelle. Pour la partir sur laquelle repose le projecteur, il suffit de créer une **Esquisse** et ensuite de l'**Extruder**. Il faut cependant prendre garde à **Associer** ce nouveau corps créé avec la partie orange. Idem pour l'ouverture sur cette même surface orange où l'**Esquisse** et l'**Extrusion** suffisent. Cependant, pour les rails aux extrémités de la partie orange, il faut **Extruder** avec une forme particulière en prenant soin de bien réaliser l'**Esquisse** préalable car la moindre erreur peut entraîner l'impossibilité de la fonction (et je sais de quoi je parle après avoir essayer meême avec l'outil **Balayage**). Pour la réalisation des encoches sur la face où se trouve les bobines, J'ai utilisé lors de l'**Esquisse** la fonction **Miroir** qui permet d'avoir la même encoche de l'autre côté une fois un axe de référence central mis en place.

![](../images/étape_5.png)

## Etape 6

Vient alors l'étape de création de la fameuse partie de demi-phère blanche restante. pour ce faire, on réalise une sphère comme lors de la première étape et on va venir utilisé l'outil **Dissocier** une nouvelle fois pour ensuite supprimer les partie qui ne nous intéresse pas. Et non seulement la fonction a permis de conserver le morceau de demi-phère qui nous intéressé, mais en plus elle s'est adapter parfaitement à la forme des encoches que nous avions travailler précédemment. Ainsi les encoches et les extrémités se lient parfaitement. Il ne nous reste alors plusqu'à utiliser l'outil **Coque** nous permettant de "vider" cette forme acquise et n'en garder qu'une coque mince où peuvent se cacher les éléments internes.

![](../images/étape_6.png)

## Cas particuliers: les boutons

Pour ce qui est des différents boutons, ils sont tous différents et fonc appel à des techniques différentes à chaque fois, que ce soit le **Lissage**, le **Réseau circulaire**, la **Révolution** ou même parfois tout ces élemtns en même temps. Cependant, pour la plupart, la façon d'agir et toujours au départ de trouver un plan qui nous semble de plus simple pour réaliser ces figures qui doivent d'ailleurs parfois être réalisés en différentes parties et jointes ensuite. Il faut toujours se demander quel est le plan ke plus avantageux ou juste plus simple et voir comment faire évoluer ces petites géométries. Il ne faut pas oublier que des fonctions telles que les **Congés** peuvent se révéler très utiles pour ce genre de réalisations.

![](../images/étape_7.png)

## Rendu final

Le final est assez satisfaisant et le fait d'avoir créé de nombreux nouveaux corps me permet ainsi de choisir les éléments que je souhaite voir apparaître ou non. C'est un objet de petite taille mais de grande complexité qui nous est offert ici et sa conception est très bien pensée afin de laisser le design épuré et simple.

![](../images/triptique.png)

![](../images/final_ouvert.png)


### Tutos Fusion 360

- [Créer une pièce simple](https://www.youtube.com/watch?v=BWSg7-TPGHs)
- [Roulement et assemblage](https://www.youtube.com/watch?v=g53MAv5jZ8k)
- [Cannelures et dentelures](https://www.youtube.com/watch?v=7SgJnH2SIBg)
