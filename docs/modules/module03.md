# 3. Impression 3D

A présent que l'objet est modélisé, on peut alors l'imprimer en 3D et se familiariser avec cette techniques qui permet un prototypage rapide. POur rexporter en **STL** via **Fusion 360**, cela se fait pas Cloud ce qui permet ainsi de ne pas en demander trop à nos ordinateurs et je trouve cette démarche très bien pensée.
Pour ce faire, il faut que le fichier **STL** passe dans un logiciel nommé **PrusaSlicer** qui va se charge de le tranformé en **G-code** lisible par la machine d'impression. Dans ce dernier logiciel, il est possible non pas de modifier l'objet, mais d'intéragir sur les paramètres d'impression tels que la présence ou non d'une jupe, de supports,...

![](../images/3_formes.png)

## Forme 1

La première étape a été pour moi de réaliser une copie de mon fichier pour ne surtout pas modifier mon modèle et le détruire à force de manipulation. Il s'agissait pour moi de réaliser trois formes différentes afin de voir les différentes possibilités de l'imprimante 3D. Pour cela, j'ai d'abord décidé de réaliser la forme basique simple sans boutons ni ornementations. Cela m'a déjà donné via le logiciel **PrusaSlicer** une idée de l'importance des supports dans mon Gcode car sans ces derniers, la partie basse de la sphère ne pourrait pas se matérialiser.

![](../images/forme1.png)

## Forme 2

J'ai ensuite souhaité réaliser la forme complète fermée de mon objet avec cette fois-ci les différents boutons pour obtenir l'objet véritable. Et là on voit que les supports se multiplient notamment puisque la partie où se trouve le rétroprojecteur est vide et ainsi des supports sont présents pour supporter la coque blanche en **ABS**. Il y a également une multiplication des interface de suport sur le bord du globe certainement pour assurer une meilleure stabilité durant l'impression.

![](../images/forme_2.png)

## Forme 3

Par la suite, j'ai tenté d'imprimer le modèle mais cette fois, toujours sans les bobines, mais également sans la coque qui renferme justement ce rétroprojecteur. Les support sont alors bien présents dans le but de pouvoir créer les embouts et différentes accroches du modèle. L'emploi d'une bordure me semblait également nécessaire afin de faire coller convenablement l'ensemble des formes au plateau et d'éviter ainsi une perte de chaleur engeandrant le plus souvent un décollement du modèle et réduisant à néant plusieurs heures d'impression.

![](../images/forme_3.png)

Ces trois formes sont une évolution de complexité afin de voir comment l'imprimante va réagir au fur et à mesure que les pièces se complexifient. Si les résultats ne sont pas probants, peut être faudra-t-il que j'imprime chaque partie séparemment et que je les assemble ensuite afin d'avoir mon produit final complet. Cela risque d'être plus long mais également d'offrir une meilleure qualité d'impression notamment pour les parties devant s'emboîter. Mais pour le moment le seul inconvénient pour moi est la présence d'autant de supports qui sont à mon avis trop fragiles et risquent de casser lors de l'impression ou si la buse les percute un peu trop fort. A voir... 
Cependant, la durée d'impression était beaucoup trop longue mais elle a pu être réduite grâce à l'intervention des fabmanageuses qui m'ont expliqué comment gagner une dizaine d'heure. L'impression reste tout de même de plus de 20 heures et il semble du coup important de bien s'organiser pour chaque changement à réaliser sur l'objet afin de ne pas se faire prendre de court par le temps.

![](../images/impression_3D.png)

