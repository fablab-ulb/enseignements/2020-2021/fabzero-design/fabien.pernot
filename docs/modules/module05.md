# 5. Usinage assisté par ordinateur

## Shaper Origin

![](../images/shaper_présentation.png)

Nous avons eu la possibilité d'avoir une formation sur la fraiseuse CNC manuelle de la marque **Shaper**. Cette découpeuse est manuelle et demande ainsi à pouvoir avoir accès à l'ensemble de la surface. 
Cette machine lit les fichiers SVG et possède un port USB afin de pouvoir placer facilement et efficacement les différents fichiers.
L'intérêt d'une telle machine se trouve dans le fait que, dans le cas d'une CNC ordinaire, il y a l'obligation de créer un gcode alors que par l'intermédiaire d'une machine comme celle-ci, il y a moyen de faire des réglages **in situ** pour les découpes et ainsi corriger rapidement des erreurs. Il y a par exemple différentes découpes possibles où l'on peut découper à l'intérieur de la ligne ce qui occasionne une perte de matière (**Inside**), à l'extérieur de la ligne afin d'avoir une pièce au dimension parfaite (**Outside**) notamment très utile dans le cas où il y aurait besoin d'imbriquer des pièces par exemple, sur la ligne elle-même (**On-line**) ou la possibilité également de creuser l'intérieur d'une pièce.
Il existe eégalement différentes vitesses manuellement changeables, il est d'ailleurs conseillé de réaliser des essais au préalable car une vitesse trop basse vas empêcher de bien fraiser et va donner des à-coups tandis qu'une vitesse trop élévée va donner des marques de brûlures et peurt endommager la fraise.

Il y a différentes étapes à suivre afin de pouvoir se servir de cette machine :

1. La première est simplement de vérifier que le plan de travail est bien stable à l'aide de serre-joint par exemple où même en vissant tout simplement ce dernier.

2. Une fois cette étape vérifié, il faut ensuite fixer la planche à travailler avec du double face pour être certain que la pièce que l'on cherche à créer ne se détache pas une fois la découpe du contour réalisée.

3. Ensuite, il est important d'appliquer sur cette surface à travailler un tape fourni par **Shaper** sur la surface avec environ 10cm d'écart entre chaque bande qui n'ont pas besoin d'être placé en parallèle. Ces bandes sont indispensable à la machine pour déterminer son espace et reconnaître la surface sur laquelle elle va travailler.

4. La machine va alors demander à lancer la reconnaissance et il faut dans ce cas manuellement déplacer la machine sur la surface afin qu'elle crée une cartographie de la surface grâce aux différentes bandes mises au préalable. On appuie sur le bouton verte placé sur le support gauche afin de signifier à la machine que le reprérage est complet.

5. Ensuite on peut alors placer la fraise. Pour cela encore d'autres étapes de sécurité sont à respecter:
- On débranche tout d'abord l'alimentation de la fraiseuse
- On enlève la vis placer sur le côté droit à l'aide de la clé prévue à cet effet 
- On enlève la fraiseuse en la faisant coulisser
- On place la fraise en maintenant fermement le bouton de blocage (elle ne doit pas être trop enfoncée ni trop peu)
- On visse ensuite le tout avec une clé tout en maintenant toujours le bouton de blocage
- On replace ensuite la fraiseuse en la faisant coulisser (sans forcer) en s'assurant que le bouton de marche arrêt est tourné vers nous
- On revisse placer sur le côté droit à l'aide de la clé prévue à cet effet
- Afin que la machine obligation puisse comprendre son axe Z, on appuie sur le bouton **Z touch** sur le panneau de commande pour qu'elle puisse descendre et comprendre à quel moment elle touche la surface à travailler

6. Il suffit ensuite de placer notre dessin sur la surface de travail grâce au panneau de commande. Pour ce faire, il faut insérer la clé USB contenant notre fichier SVG et le sélectionner pour ensuite le placer manuellement en déplaçant la machine sur la surface. Une fois une position satisfaisante trouvée, le tout est alors d'appuyer sur le bouton vert de la poignée de droite afin de fixer l'emplacement. A ce moment, la machine se souviendra de l'emplacement du fichier sur la surface.
Attention lors du placement que l'intégralité de l'image se trouve dans lespace reconnu par la machine grâce au tape **Shaper** car sinon un icône en haut à droite deviendra rouge et la machine refusera de fraiser ou graver.
7. Il faut à présent passer en **Cut mode** et se déplacer sur le dessin du fichier 

8. On remarque alors que les lignes apparaissent en noir sur l'écran de commande de la **Shaper** et il faut alors lui indiquer quelle fraise a été mise en place et à quelle distance on souhaite qu'elle perfore. En effet, cela dépend si l'on peut simplement graver ou fraiser l'objet. On sélectionne également à ce moment là les différentes options **Inside**, **Outside**, **On-line** ou à l'intérieur pour indiquer à la machine comment creuser l'objet.

9. On branche alors l'aspirateur relié par un tuyau au côté gauche de la machine

10. On règle la vitesse de la fraiseuse (3 pour la gravure et 5 pour le fraisage)

11. On allume la fraiseuse grâce au bouton On/Off situé à l'avant de cette dernière.

12. On appuie sur le bouton vert sur la poignée droite afin de lancer la phase de découpe et la fraiseuse va alors descendre afin que l'on puisse fraiser.

13. On suit alors le sens de la ligne de découpe suggérée par la machine qui va se charger de fraiser avec précision et corriger nos erreurs même si nous-mêmes nous ne sommes pas précis.

14. Une fois une forme terminée. Il faut toujours appouyer sur le bouton droit situé sur la poignée de droite afin que la fraiseuse remonte pour ensuite passer à la forme suivante. Il est conseillé de faire plusieurs passages afin d'avoir un produit final impeccable. 

15. Une fois la découpe terminée, on éteint la fraiseuse grâce au bouton On/Off situé à l'avant de cette dernière, on éteint l'aspirateur, on enlève la machine pour récupérer la pièce obtenue et on range le matériel.

![](../images/shaper_étapes.png)

Toutes les étapes de cette utilisation sont disponible via le site même de **Shaper** qui offre une série de tutoriel utiles afin de comprendre l'utilité et l'utilisation d'une telle machine.

![](../images/ShaperOriginLogo.png)

[Site de Shaper](https://www.shapertools.com/)

[Tuto Air Cut](https://www.youtube.com/watch?v=sdsMOPZFMRQ&feature=emb_logo)

[Tuto First Cut](https://www.youtube.com/watch?v=DekAjAOIVvQ&feature=emb_logo)

[Tuto Fusion 360 à partir d'un SVG](https://www.youtube.com/watch?v=1T8kDh-N0yA&feature=emb_logo)

[Tuto Fusion 360 conversion d'un DXF](https://www.youtube.com/watch?v=tQ27OCaqVlA&feature=emb_logo)

[Tuto Fusion 360 projet à partir d'un croquis](https://www.youtube.com/watch?v=oCk2a8Axd1E&feature=emb_logo)
